// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
  require('../src/components/Button.stories')
  require('../src/screens/SignIn/SignIn.stories')
}

const stories = [
  '../src/components/Button.stories',
  '../src/screens/SignIn/SignIn.stories',
]

module.exports = {
  loadStories,
  stories,
}
