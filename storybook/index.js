import {Navigation} from 'react-native-navigation'
import {getStorybookUI, configure, addDecorator} from '@storybook/react-native'
import {withKnobs} from '@storybook/addon-knobs'
import {loadStories} from './storyLoader'

import './rn-addons'

// enables knobs for all stories
addDecorator(withKnobs)

// import stories
configure(() => {
  loadStories()
}, module)

const StorybookUIRoot = getStorybookUI({})

export const registerStorybook = () => {
  Navigation.registerComponent('storybook.UI', () => StorybookUIRoot)
}

export const onStartStorybook = () => {
  Navigation.setRoot({
    root: {
      component: {
        name: 'storybook.UI',
      },
    },
  })
}
