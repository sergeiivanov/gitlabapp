import '@emotion/react'
import {Color, FontSize, FontFamily, Space} from '@/theme'
declare module '@emotion/react' {
  export interface Theme {
    colors: {[key in Color]: string}
    fontSizes: {[key in FontSize]: string}
    fontFamilies: {[key in FontFamily]: string}
    space: {[key in Space]: string}
  }
}
