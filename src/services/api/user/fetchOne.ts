import api from '../instance'
import handleError from '@/utils/errorHandler'
import {config} from '@/config'

export default async (token: string) => {
  if (!token) {
    return handleError({message: 'Access Token is required'})
  }
  const response = await api.get(`${config.API_URL}/user`, {
    headers: {'PRIVATE-TOKEN': token},
  })
  return response.data
}
