import React from 'react'
import {NavigationFunctionComponent} from 'react-native-navigation'
import {Text} from 'react-native'
import styled from '@emotion/native'
import {useUser} from '@/hooks'

const Container = styled.SafeAreaView`
  flex: 1;
  padding-bottom: 40px;
  justify-content: space-between;
  align-items: center;
  background-color: ${props => props.theme.colors.backgroundDefault};
`

const Home: NavigationFunctionComponent = ({componentId}) => {
  const {userInfo} = useUser()

  return (
    <Container>
      <Text>{'Profile'}</Text>
    </Container>
  )
}

export default Home
