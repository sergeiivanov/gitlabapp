import {Navigation} from 'react-native-navigation'
import {SignIn} from './SignIn'
import {Home} from './Profile'
import {persistor, store} from '@/store'

export const SIGN_IN_SCREEN = 'gitlabapp.SignIn'
export const PROFILE_SCREEN = 'gitlabapp.Profile'

export const Screens = new Map()
Screens.set(SIGN_IN_SCREEN, SignIn)
Screens.set(PROFILE_SCREEN, Home)

export const onStartApp = () => {
  persistor.subscribe(() =>
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: store.getState().user.isAuth
                  ? PROFILE_SCREEN
                  : SIGN_IN_SCREEN,
              },
            },
          ],
        },
      },
    }),
  )
}
