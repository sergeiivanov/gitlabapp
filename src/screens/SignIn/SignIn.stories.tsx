import React from 'react'
import {storiesOf} from '@storybook/react-native'
import {Provider} from 'react-redux'
import {ThemeProvider} from '@emotion/react'
import {theme} from '@/theme'
import {store} from '@/store'

import {default as SignIn} from './SignInContainer'

storiesOf('Home Scrren', module).add('Init', () => (
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <SignIn componentId={''} />
    </Provider>
  </ThemeProvider>
))
