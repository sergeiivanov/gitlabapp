import React, {useEffect} from 'react'
import {Navigation, NavigationFunctionComponent} from 'react-native-navigation'
import {Alert} from 'react-native'
import styled from '@emotion/native'
import {Button} from '@/components'
import {useUser} from '@/hooks'
import {PROFILE_SCREEN} from './../index'

const Container = styled.SafeAreaView`
  flex: 1;
  padding-bottom: 40px;
  justify-content: space-between;
  align-items: center;
  background-color: ${props => props.theme.colors.backgroundDefault};
`

const TextContainer = styled.View`
  padding: 20px;
`
const Header = styled.Text`
  color: #000;
  font-size: 32px;
  font-weight: 700;
  margin-bottom: 20px;
`

const DefaultText = styled.Text`
  color: #000;
  font-size: 16px;
  font-weight: 400;
`

const Spacer = styled.View`
  height: 10px;
  width: 100%;
`

const SignIn: NavigationFunctionComponent = ({componentId}) => {
  const {getUser, clearUserError, userLoading, userError, userSuccess} =
    useUser()

  useEffect(() => {
    if (userSuccess) {
      Navigation.setStackRoot(componentId, {
        component: {
          name: PROFILE_SCREEN,
        },
      })
    }
  }, [userSuccess, componentId])

  useEffect(() => {
    if (userError)
      Alert.alert('', userError, [
        {
          text: 'OK',
          onPress: clearUserError,
        },
      ])
  }, [userError, clearUserError])

  const enterToken = () => {
    Alert.prompt('Access Token', 'Enter your access token', [
      {
        text: 'Cancel',
        style: 'cancel',
      },
      {
        text: 'Submit',
        onPress: (token = '') => getUser(token),
      },
    ])
  }

  return (
    <Container>
      <TextContainer>
        <Header>{'Create a personal access token'}</Header>
        <DefaultText>{'1. In your acctount, select Edit profile.'}</DefaultText>
        <DefaultText>
          {'2. On the left sidebar, select Access Tokens.'}
        </DefaultText>
        <DefaultText>
          {'3. Enter a name and optional expiry date for the token.'}
        </DefaultText>
        <DefaultText>
          {'4. Select the desired scopes (api, read_user, read_repository).'}
        </DefaultText>
        <DefaultText>{'5. Select Create personal access token.'}</DefaultText>
        <Spacer />
        <DefaultText>
          {
            'Save the personal access token somewhere safe. After you leave the page, you no longer have access to the token.'
          }
        </DefaultText>
      </TextContainer>
      <Button loading={userLoading} onPress={enterToken}>
        {'Sign in with Access Token'}
      </Button>
    </Container>
  )
}

export default SignIn
