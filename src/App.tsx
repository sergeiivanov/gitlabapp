import React from 'react'
import {Navigation} from 'react-native-navigation'
import {Provider} from 'react-redux'
import {ThemeProvider} from '@emotion/react'
import {STORYBOOK_LOAD} from '@env'
import {store} from '@/store'
import {Screens, onStartApp} from '@/screens'
import {theme} from '@/theme'
import {registerStorybook, onStartStorybook} from '../storybook'

if (__DEV__ && STORYBOOK_LOAD === 'true') {
  registerStorybook()

  Navigation.events().registerAppLaunchedListener(() => {
    onStartStorybook()
  })
} else {
  Screens.forEach((ScreenComponent, key) => {
    Navigation.registerComponent(
      key,
      () => props =>
        (
          <ThemeProvider theme={theme}>
            <Provider store={store}>
              <ScreenComponent {...props} />
            </Provider>
          </ThemeProvider>
        ),
      () => ScreenComponent,
    )
  })

  Navigation.events().registerAppLaunchedListener(() => {
    onStartApp()
  })
}
