import React from 'react'
import {storiesOf} from '@storybook/react-native'
import {action} from '@storybook/addon-actions'

import {BufferView} from '@/utils/decorators.stories'
import {Button} from './Button'

storiesOf('Button Primary', module)
  .addDecorator(BufferView)
  .add('Default', () => (
    <Button onPress={action('tapped-primary')}>
      {'Sign in with Access Token'}
    </Button>
  ))
  .add('Loading', () => (
    <Button disabled={true} loading={true} onPress={action('tapped-primary')}>
      {'Sign in with Access Token'}
    </Button>
  ))
