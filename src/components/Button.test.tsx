import React from 'react'
import {fireEvent, render} from '@testing-library/react-native'
import {Button} from './Button'

describe('Button Primary', () => {
  it('- get title and press', async () => {
    const {getByText} = render(
      <Button onPress={() => {}}>{'Sign in with Acces Token'}</Button>,
    )
    const button = getByText(/Sign in/i)
    fireEvent.press(button)
  })
})
