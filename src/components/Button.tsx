import React from 'react'
import {ActivityIndicator} from 'react-native'
import styled from '@emotion/native'

const TouchableOpacity = styled.TouchableOpacity`
  height: 40px;
  width: 80%;
  justify-content: center;
  align-items: center;
  background-color: #000;
  border-radius: 20px;
`
const Text = styled.Text`
  color: #fff;
  font-size: 16px;
  font-weight: 700;
`

export const Button = ({
  onPress = () => {},
  children = '',
  disabled = false,
  loading = false,
}) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress} activeOpacity={0.9}>
      {loading ? (
        <ActivityIndicator animating={loading} size="small" color="#fff" />
      ) : (
        <Text>{children}</Text>
      )}
    </TouchableOpacity>
  )
}
