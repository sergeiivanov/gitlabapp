import React from 'react'
import styled from '@emotion/native'
import {StoryFn} from '@storybook/addons'

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`

export const BufferView = (storyFn: StoryFn<React.ReactNode>) => (
  <Container>{storyFn()}</Container>
)
