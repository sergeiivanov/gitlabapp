import {useSelector, useDispatch} from 'react-redux'
import {fetchUser, clearUserError as clearError} from '@/store/slices/user'

export function useUser() {
  const dispatch = useDispatch()
  const userInfo = useSelector(state => state.user.info)
  const userLoading = useSelector(state => state.user.loading)
  const userError = useSelector(state => state.user.error)
  const userSuccess = useSelector(state => state.user.success)

  function getUser(token: string) {
    dispatch(fetchUser(token))
  }

  function clearUserError() {
    dispatch(clearError())
  }

  return {
    getUser,
    userInfo,
    userLoading,
    userError,
    userSuccess,
    clearUserError,
  }
}
