import {createSlice} from '@reduxjs/toolkit'

interface ThemeState {
  theme: string | null
  darkMode: boolean | null
}

interface PayloadInterface {
  payload: Partial<ThemeState>
}

const themeSlice = createSlice({
  name: 'theme',
  initialState: {
    theme: null,
    darkMode: null,
  },
  reducers: {
    changeTheme(state: ThemeState, {payload}: PayloadInterface) {
      if (typeof payload.theme !== 'undefined') {
        state.theme = payload.theme
      }
      if (typeof payload.darkMode !== 'undefined') {
        state.darkMode = payload.darkMode
      }
    },
  },
})

export const {changeTheme} = themeSlice.actions
export default themeSlice.reducer
