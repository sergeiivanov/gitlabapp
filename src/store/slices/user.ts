import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import {fetchOneUserService} from '@/services/api/user'

interface Info {
  id: number
}

interface IUser {
  loading: boolean
  info: Info | null
  error: string | null | undefined
  success: boolean
  isAuth: boolean
  token: string | null
}

export const fetchUser = createAsyncThunk<Info, string>(
  'user/fetchOneUser',
  async token => {
    const response = await fetchOneUserService(token)
    return response
  },
)

const userSlice = createSlice({
  name: 'user',
  initialState: {
    isAuth: false,
    token: null,
    loading: false,
    info: null,
    error: null,
    success: false,
  } as IUser,
  reducers: {
    clearUserError(state: IUser) {
      state.error = null
    },
  },
  extraReducers: builder => {
    builder.addCase(fetchUser.pending, state => {
      state.loading = true
      state.error = null
      state.success = false
    })
    builder.addCase(fetchUser.fulfilled, (state, action) => {
      state.info = action.payload
      state.loading = false
      state.success = true
      state.isAuth = true
      state.token = action.meta.arg
    })
    builder.addCase(fetchUser.rejected, (state, action) => {
      state.error = action.error.message
      state.loading = false
    })
  },
})

export const {clearUserError} = userSlice.actions
export default userSlice.reducer
