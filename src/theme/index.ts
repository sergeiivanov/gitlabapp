const pallete = {
  black: '#000000',
  white: '#FFFFFF',
  pink: 'pink',
}

const colors = {
  textDefault: pallete.white,
  backgroundDefault: pallete.white,
}

const fontSizes = {
  medium: '16px',
  large: '32px',
}

const fontFamilies = {
  light: '',
  regular: '',
  bold: '',
}

const space = {
  small: '4px',
  medium: '12px',
  large: '36px',
}

export type Color = keyof typeof colors
export type FontSize = keyof typeof fontSizes
export type FontFamily = keyof typeof fontFamilies
export type Space = keyof typeof space
export const theme = {
  colors,
  fontSizes,
  fontFamilies,
  space,
}
